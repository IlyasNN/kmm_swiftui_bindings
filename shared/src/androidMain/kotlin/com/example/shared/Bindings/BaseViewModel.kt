package com.example.shared.Bindings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

actual open class BaseViewModel: ViewModel() {
    actual val clientScope: CoroutineScope = viewModelScope
    actual val _isLoading = MutableStateFlow<Boolean>(false)
    actual val isLoading: StateFlow<Boolean>
        get() = _isLoading
}