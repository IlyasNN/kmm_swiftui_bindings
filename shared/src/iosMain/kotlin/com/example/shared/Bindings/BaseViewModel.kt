package com.example.shared.Bindings

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlin.coroutines.CoroutineContext

actual open class BaseViewModel {
    actual val clientScope: CoroutineScope = CoroutineScope(object : CoroutineContext {
        override fun <R> fold(initial: R, operation: (R, CoroutineContext.Element) -> R): R {
            return initial
        }

        override fun <E : CoroutineContext.Element> get(key: CoroutineContext.Key<E>): E? {
            return null
        }

        override fun minusKey(key: CoroutineContext.Key<*>): CoroutineContext {
            return this
        }

    })
    actual val _isLoading: MutableStateFlow<Boolean> = MutableStateFlow(false)
    actual val isLoading: StateFlow<Boolean>
        get() = _isLoading
}