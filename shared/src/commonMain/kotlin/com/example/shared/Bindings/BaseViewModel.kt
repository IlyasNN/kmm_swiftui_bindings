package com.example.shared.Bindings

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

expect open class BaseViewModel() {
    val clientScope: CoroutineScope
    val _isLoading: MutableStateFlow<Boolean>
    val isLoading: StateFlow<Boolean>
}