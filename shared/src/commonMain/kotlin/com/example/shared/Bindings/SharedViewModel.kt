package com.example.shared.Bindings

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.datetime.*

class SharedViewModel(): BaseViewModel() {
    val stringValue = CommonStateFlow(MutableStateFlow<String>("Default value")).asCommonFlow()
    val boolValue = CommonStateFlow(MutableStateFlow<Boolean>(true)).asCommonFlow()
    val minSliderValue: Double = -100.0
    val maxSliderValue: Double = 100.0
    val doubleValue = CommonStateFlow(MutableStateFlow<Double>(70.0)).asCommonFlow()
    var pickerValues = listOf("value1", "value2", "value3", "value4")
    val selectedPickerValue = CommonStateFlow(MutableStateFlow<String>("value1")).asCommonFlow()
    val now: Instant = Clock.System.now()
    val dateValue = CommonStateFlow(MutableStateFlow<LocalDate>(now.toLocalDateTime(TimeZone.currentSystemDefault()).date)).asCommonFlow()
}