import shared

extension DateComponents {
    
    init(_ kotlinLocaleDate: Kotlinx_datetimeLocalDate) {
        self.init(year: Int(kotlinLocaleDate.year),
                  month: Int(kotlinLocaleDate.monthNumber),
                  day: Int(kotlinLocaleDate.dayOfMonth))
    }
    
}
