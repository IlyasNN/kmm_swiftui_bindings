import SwiftUI
import shared

extension Toggle where Label == Text {
    
    init(_ titleKey: LocalizedStringKey,
         isOn: Binding<Bool>,
         flow: CommonStateFlow<KotlinBoolean>) {
        self.init(titleKey, isOn: isOn.onChange({ newValue in
            flow.emit(value: KotlinBoolean(value: newValue)) { _, _ in
                return
            }
        }))
    }
    
}
