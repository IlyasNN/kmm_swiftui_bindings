import SwiftUI
import shared

extension Picker where Label == Text {
    
    init(_ titleKey: LocalizedStringKey,
         selection: Binding<SelectionValue>,
         stringFlow: CommonStateFlow<NSString>,
         @ViewBuilder content: () -> Content) where SelectionValue: Comparable {
        self.init(titleKey,
                  selection: selection.onChange({ newValue in
            stringFlow.emit(value: (newValue as! NSString)) { _, _ in
                return
            }
        }),
                  content: content)
    }
    
}
