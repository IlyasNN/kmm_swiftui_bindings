import SwiftUI
import shared

extension DatePicker {
    
    init(selection: Binding<Date>, displayedComponents:
         DatePicker<Label>.Components = [.hourAndMinute, .date],
         flow: CommonStateFlow<Kotlinx_datetimeLocalDate>,
         @ViewBuilder label: () -> Label) {
        self.init(selection: selection.onChange({ newValue in
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year, .month, .day], from: newValue)
            guard let year = components.year,
                  let month = components.month,
                  let day = components.day else {
                      return
                  }
            
            let yearInt = Int32(year)
            let monthInt = Int32(month)
            let dayInt = Int32(day)
            
            let kotlinDate = Kotlinx_datetimeLocalDate(year: yearInt,
                                                       monthNumber: monthInt,
                                                       dayOfMonth: dayInt)
            flow.emit(value: kotlinDate) { _, _ in
                return
            }
        }),
                  displayedComponents: displayedComponents,
                  label: label)
    }
    
}
