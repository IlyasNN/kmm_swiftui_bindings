import SwiftUI

extension Text {
    
    init(_ nsString: NSString?) {
        self.init(String(nsString ?? ""))
    }
    
}
