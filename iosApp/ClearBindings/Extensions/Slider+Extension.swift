import SwiftUI
import shared

extension Slider  {

    init<V>(value: Binding<V>,
            in bounds: ClosedRange<V>,
            flow: CommonStateFlow<KotlinDouble>) where
    V : BinaryFloatingPoint,
    V.Stride : BinaryFloatingPoint,
    Label == EmptyView,
    ValueLabel == EmptyView {
        self.init(value: value.onChange({ newValue in
            flow.emit(value: KotlinDouble(value: Double(newValue))) { _, _ in
                return
            }
        }),
                  in: bounds)
    }
    
}
