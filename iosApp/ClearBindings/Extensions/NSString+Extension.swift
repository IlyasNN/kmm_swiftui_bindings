import Foundation

extension NSString {
    
    var stringValue: String {
        return String(self)
    }
    
}
