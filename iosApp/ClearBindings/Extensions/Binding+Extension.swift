import SwiftUI

extension Binding {
    
    func onChange<T: Comparable>(_ handler: @escaping (T) -> Void) -> Binding<T> {
        Binding<T>(
            get: { self.wrappedValue as! T },
            set: { newValue in
                if self.wrappedValue as! T  != newValue {
                    handler(newValue)
                }
                self.wrappedValue = newValue as! Value
            }
        )
    }
    
}
