import SwiftUI
import shared

extension TextField where Label == Text {
    
    init(_ titleKey: LocalizedStringKey,
         text: Binding<String>,
         flow: CommonStateFlow<NSString>) {
        self.init(titleKey, text: text.onChange({ newValue in
            flow.emit(value: newValue as NSString) { _, _ in
                return
            }
        }))
    }
    
}
