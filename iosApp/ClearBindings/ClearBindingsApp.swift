import SwiftUI
import shared

@main
struct ClearBindingsApp: App {
    var body: some Scene {
        WindowGroup {
            ExampleView(viewModel: SharedViewModel())
        }
    }
}
