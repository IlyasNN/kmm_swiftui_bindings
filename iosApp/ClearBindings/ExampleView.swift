import SwiftUI
import shared

struct ExampleView: View {
    
    let viewModel: SharedViewModel
    
    @State private var textFieldString: String = ""
    @State private var toggleIsOn: Bool = true
    @State private var sliderValue: Double = 0
    @State private var selectedPickerValue = ""
    @State private var date = Date()
    
    var body: some View {
        ScrollView {
            
            Text(Greeting().greeting())
                .padding()
            
            // MARK: Textfield
            Group {
                Text("Textfield").bold()
                TextField("Binded textfield",
                          text: $textFieldString,
                          flow: viewModel.stringValue)
                    .padding(16)
                Text(viewModel.stringValue.value)
            }
            
            // MARK: Toggle
            Group {
                Spacer().frame(height: 64)
                Text("Toggle").bold()
                Toggle("Binded toggle",
                       isOn: $toggleIsOn,
                       flow: viewModel.boolValue)
                    .padding(16)
                Text("\(String(viewModel.boolValue.value!.boolValue))")
            }
            
            // MARK: Slider
            Group {
                Spacer().frame(height: 64)
                Text("Slider").bold()
                Slider(value: $sliderValue,
                       in: viewModel.minSliderValue...viewModel.maxSliderValue,
                       flow:
                        viewModel.doubleValue)
                    .padding(16)
                Text("\(viewModel.doubleValue.value!.doubleValue)")
            }
            
            // MARK: Picker
            Group {
                Spacer().frame(height: 64)
                Text("Picker").bold()
                Picker("Please choose a value",
                       selection: $selectedPickerValue,
                       stringFlow: viewModel.selectedPickerValue) {
                    ForEach(viewModel.pickerValues, id: \.self) {
                        Text($0)
                    }
                }
                Text(viewModel.selectedPickerValue.value)
            }
            
            // MARK: SegmentedControl
            Group {
                Spacer().frame(height: 64)
                Text("Segmented control").bold()
                Picker("Please choose a value",
                       selection: $selectedPickerValue,
                       stringFlow: viewModel.selectedPickerValue) {
                    ForEach(viewModel.pickerValues, id: \.self) {
                        Text($0)
                    }
                }
                       .pickerStyle(.segmented)
                       .padding(16)
                Text(viewModel.selectedPickerValue.value)
            }
            
            // MARK: DatePicker
            Group {
                Spacer().frame(height: 64)
                Text("Date picker").bold()
                DatePicker(selection: $date,
                           displayedComponents: .date,
                           flow: viewModel.dateValue) {
                    Text("Select a date")
                }
                           .padding(16)
                Text("Date is \(date.formatted(date: .long, time: .omitted))")
            }
            
        }
        
        .onAppear {
            setup()
        }
    }
    
    func setup() {
        textFieldString = viewModel.stringValue.value!.stringValue
        toggleIsOn = viewModel.boolValue.value!.boolValue
        sliderValue = viewModel.doubleValue.value!.doubleValue
        selectedPickerValue = viewModel.selectedPickerValue.value!.stringValue
        
        let calendar = Calendar(identifier: .gregorian)
        let components = DateComponents(viewModel.dateValue.value!)
        date = calendar.date(from: components)!
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ExampleView(viewModel: SharedViewModel())
    }
}
